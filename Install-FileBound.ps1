param (
    # Input Parameters
    [string]$FbRepo,
    [string]$FsUser,
    [string]$FsPasswd,
    [string]$IisUser,    
    [string]$IisPasswd,
    [string]$SqlServer,
    [string]$SqlDb,
    [string]$SqlUser,
    [string]$SqlPasswd,
    [string]$FbVersion
)

function UrlToUnc {
    param (
        [string]$url
    )

    $fileshare = $customer.url.split("/")
    $uncpath = "\\" + $fileshare[-2] + "\" + $fileshare[-1]

    return $uncpath
}

function Base64ToString {
    param (
        [string]$b64str
    )

    $b = [System.Convert]::FromBase64String($b64str)
    $result = [System.Text.Encoding]::UTF8.GetString($b)

    return $result
}

$FbRepo = Base64ToString($FbRepo)
$FsUser = Base64ToString($FsUser)

net use z: $FbRepo "/u:$FsUser" "$FsPasswd"

try {
    Copy-Item "$FbRepo\FileBound\ProvisioningUtils\FileBoundStorageManager" -Destination "C:\Program Files\FileBound Storage Manager\" -Recurse -Force:$true
}
catch {
    Add-Content C:\Deploy\provisioning.txt "Error: Copy FileBoundStorageManager failed."
    $_.Exception.Message
    exit $errorcode
}
Set-Location "C:\Program Files\FileBound Storage Manager\"
$configFile = "FileBoundStorageManager.exe.config"
(Get-Content $configFile).Replace('xxx_changeme_xxx', $SqlDb) | Set-Content $configFile
C:\Windows\Microsoft.NET\Framework64\v4.0.30319\InstallUtil.exe FileBoundStorageManager.exe


try {
    Copy-Item "$FbRepo\FileBound\ProvisioningUtils\Enable-LogonAsService.ps1" -Destination "C:\Deploy\"  -Force:$true
}
catch {
    Add-Content C:\Deploy\provisioning.txt "Error: Copy Enable-LogonAsService.ps1 failed."
    $_.Exception.Message
    exit $errorcode
}

try {
    Copy-Item "$FbRepo\FileBound\ProvisioningUtils\Config-FileBound.ps1" -Destination "C:\Deploy\"  -Force:$true
}
catch {
    Add-Content C:\Deploy\provisioning.txt "Error: Copy Config-FileBound.ps1 failed."
    $_.Exception.Message
    exit $errorcode
}

# Create a local service account for IIS 
$Password = ConvertTo-SecureString -AsPlainText $IisPasswd -Force
New-LocalUser $IisUser -Password $Password -PasswordNeverExpires -UserMayNotChangePassword -Description "IIS Service Account"
Add-LocalGroupMember -Group "Users" -Member $IisUser

# Give user permission to inetpub
$ThisPath = "C:\inetpub"
$PathAcl= = Get-Acl $ThisPath
$AclEntry = "$IisUser","Modify","ContainerInherit, ObjectInherit","None","Allow"
$AclRule = New-Object System.Security.AccessControl.FileSystemAccessRule($AclEntry)
$PathAcl.SetAccessRule($AclRule)
$PathAcl | Set-Acl $ThisPath

C:\Deploy\Config-FileBound.ps1 `
   -fbrepo $FbRepo `
   -storage_acc_secret $FsPasswd `
   -storage_acc_username $FsUser `
   -fbversion $FbVersion.trim() `
   -iisUserName $IisUser `
   -iisPassword $IisPasswd `
   -sqlServer $SqlServer `
   -sqlDbName $SqlDb `
   -sqlUserName $SqlUser `
   -sqlPassword $SqlPasswd


sc.exe config "FileBoundStorageManager" obj= ".\svc-iis" password= "$IisPasswd"
C:\Deploy\Enable-LogonAsService.ps1 "svc-iis"
Start-Service -Name FileBoundStorageManager
